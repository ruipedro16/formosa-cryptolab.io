---
layout: default

title: Swarn Priya wins L'Oréal-UNESCO 2023 prize
date: 2023-10-09

summary: >-
  [Swarn Priya](https://swarnpriya.github.io) won the Young Talents "Pour les
  femmes et la science" L'Oréal-Unesco 2023 prize.
  More information at [https://www.inria.fr/en/certifying-correctness-security-and-reliability-software-swarn-priya-wins-loreal-unesco-2023-prize](https://www.inria.fr/en/certifying-correctness-security-and-reliability-software-swarn-priya-wins-loreal-unesco-2023-prize).
---

# Swarn Priya wins L'Oréal-UNESCO 2023 prize

More information at [https://www.inria.fr/en/certifying-correctness-security-and-reliability-software-swarn-priya-wins-loreal-unesco-2023-prize](https://www.inria.fr/en/certifying-correctness-security-and-reliability-software-swarn-priya-wins-loreal-unesco-2023-prize).
