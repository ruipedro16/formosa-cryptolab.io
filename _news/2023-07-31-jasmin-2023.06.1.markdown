---
layout: default

title: Jasmin release 2023.06.1
date: 2023-07-31

summary: >-
  A new minor version of Jasmin is available.
linkback: >-
  Read the announcement.
---

# Release 2023.06.1 of the Jasmin compiler

A new minor version of the Jasmin compiler has just been released. It brings in
a few improvements to the (still experimental) support for ARMv7 architecture.
Some issues in the semantics description of the target architectures have been
found through random testing and are now fixed. Finally, global arrays of bytes
can be conveniently written as literal strings.

Relevant details can be found through the
[CHANGELOG](https://github.com/jasmin-lang/jasmin/blob/v2023.06.1/CHANGELOG.md).
