---
layout: page
title: People

permalink: /people/
---

{% for person in site.people %}
- [**{{ person.name }}**]({{ person.website }}) ({{ person.institution }})<br/>
  {% if person.projects %} Projects: {{ person.projects }}<br/> {% endif %}
  {{ person.content | markdownify }}
{% endfor %}
