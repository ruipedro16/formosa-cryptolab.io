---
name: Alley Stoughton
institution: Boston University
website: https://alleystoughton.us

projects: EasyCrypt
---

[Alley](https://alleystoughton.us) is a contributor to and user of
EasyCrypt. Her current projects include [mechanizing proofs of
Universally Composable (UC) security in
EasyCrypt](https://github.com/easyuc/EasyUC) and [formalizing
algorithmic bounds using
EasyCrypt](https://github.com/alleystoughton/AlgorithmicBounds).
